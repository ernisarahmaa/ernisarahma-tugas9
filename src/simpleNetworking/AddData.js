import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Modal,
  Alert,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {TOKEN, BASE_URL} from './url';
import axios from 'axios';
import ApiProvider from '../data/api/ApiProvider';

const AddData = ({showModal, onCloseModal, selected, setSelected}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  // const headers = {
  //   headers: {
  //     'Content-Type': 'application/json',
  //     Authorization: TOKEN,
  //   },
  // };

  useEffect(() => {
    if (selected) {
      const data = selected;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, [showModal]);

  const postData = async () => {
    setIsLoading(true);
    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    const res = await ApiProvider.addMobil(body);
    if (res.status === 200 || res.status === 201) {
      alert('Data Mobil berhasil ditambah');
      onCloseModal();
    } else {
      alert('Data Mobil Gagal Ditambah');
    }
    // axios
    //   .post(`${BASE_URL}mobil`, body, headers) // wajib body dlu baru header
    //   .then(response => {
    //     console.log('response success: ', response);
    //     if (response.status === 200 || response.status === 201) {
    //       Alert.alert('Data Mobil berhasil ditambahkan');
    //       resetData();
    //       onCloseModal();
    //     }
    //   })
    //   .catch(error => console.log('error add data: ', error));
  };

  const editData = async () => {
    setIsLoading(true);
    const body = [
      {
        _uuid: selected._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    const res = await ApiProvider.editMobil(body);
    if (res.status) {
      alert('Data Mobil Gagal Dirubah');
    } else {
      alert('Data Mobil berhasil dirubah');
    }
    setIsLoading(false);
    onCloseModal();
    // axios
    //   .put(`${BASE_URL}mobil`, body, headers)
    //   .then(response => {
    //     if (response.status === 200 || response.status === 201) {
    //       Alert.alert('Data Mobil berhasil dirubah');
    //       resetData();
    //       onCloseModal();
    //     }
    //   })
    //   .catch(error => console.log('error update data: ', error));
  };

  const deleteData = async () => {
    setIsLoading(true);
    const body = [
      {
        _uuid: selected._uuid,
      },
    ];

    const res = await ApiProvider.deleteMobil(body);
    if (res.status === 200 || res.status === 201) {
      alert('Data Mobil berhasil dihapus');
      onCloseModal();
    } else {
      alert('Data Mobil Gagal Dihapus');
    }
    // axios
    //   .delete(`${BASE_URL}mobil`, {data: body, ...headers})
    //   .then(response => {
    //     if (response.status === 200 || response.status === 201) {
    //       Alert.alert('Data Mobil berhasil dihapus');
    //       resetData();
    //       onCloseModal();
    //     }
    //   })
    //   .catch(error => console.log('error delete data: ', error));
  };

  return (
    <Modal
      visible={showModal}
      transparent={true}
      onRequestClose={() => {
        Alert.alert('Modal has been closed.');
        onCloseModal();
        setSelected(null);
      }}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.4)',
        }}>
        <View
          style={[
            {
              width: 300,
              backgroundColor: '#fff',
              justifyContent: 'center',
              borderRadius: 5,
            },
            selected ? {height: 500} : {height: 450},
          ]}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{
                width: '10%',
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 10,
                marginLeft: 10,
                marginRight: 15,
              }}
              onPress={() => {
                onCloseModal(false);
                setSelected(null);
              }}>
              <Icon name="times" size={20} color="#000" />
            </TouchableOpacity>
            <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
              {selected ? 'Edit Data' : 'Tambah Data'}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              padding: 15,
            }}>
            <View>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Nama Mobil
              </Text>
              <TextInput
                placeholder="Masukkan Nama Mobil"
                style={styles.txtInput}
                onChangeText={text => setNamaMobil(text)}
                value={namaMobil}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Total Kilometer
              </Text>
              <TextInput
                placeholder="contoh: 100 KM"
                style={styles.txtInput}
                onChangeText={text => setTotalKM(text)}
                value={totalKM}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Harga Mobil
              </Text>
              <TextInput
                placeholder="Masukkan Harga Mobil"
                style={styles.txtInput}
                keyboardType="number-pad"
                onChangeText={text => setHargaMobil(text)}
                value={hargaMobil}
              />
            </View>
            <TouchableOpacity
              style={styles.btnAdd}
              onPress={() => (selected ? editData() : postData())}>
              <Text style={{color: '#fff', fontWeight: '600'}}>
                {selected ? 'Edit Data' : 'Tambah Data'}
              </Text>
            </TouchableOpacity>
            {selected && (
              <TouchableOpacity
                style={[styles.btnAdd, {backgroundColor: 'red'}]}
                onPress={() => deleteData()}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>Delete</Text>
              </TouchableOpacity>
            )}
            <Modal visible={isLoading} transparent={true}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'rgba(0,0,0,0.4)',
                }}>
                <View
                  style={{
                    backgroundColor: 'white',
                    padding: 10,
                    borderRadius: 10,
                  }}>
                  <ActivityIndicator size="large" />
                  <Text style={{alignSelf: 'center', marginTop: 10}}>
                    Loading
                  </Text>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
