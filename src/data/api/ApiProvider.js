import {TOKEN} from '../../simpleNetworking/url';
import API from './ApiConfig';

export default {
  getMobil: async () => {
    return API('mobil', {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        return response;
      })
      .catch(err => {
        return err;
      });
  },

  addMobil: async () => {
    return API('mobil', {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
      body: mobil,
    });
  },

  editMobil: async () => {
    return API('mobil', {
      method: 'PUT',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
      body: mobil,
    });
  },

  deleteMobil: async mobil => {
    return API('mobil', {
      method: 'DELETE',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
      body: mobil,
    });
  },
};
